# this image is based on offical python image
FROM python

# getting all project's files
COPY . .

# install all packages
RUN pip install -r requirements.txt
RUN pip install --trusted-host nexus.owls.shlomke.xyz -i http://nexus.owls.shlomke.xyz/repository/unity/simple exporterutilities

# running the application
CMD python jira_exporter.py ; python zephyr_exporter.py
