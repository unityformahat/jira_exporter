from atlassian import Jira  # atlassian api tool for python
import pprint  # This is printing json nicely
#from exporterutilities.DbMannager import DbMannager
import requests




# Using json.dumps for converting json query to strings

def get_jira():
    """Returns Jira object. (initializing URL, USERNAME, PASSWORD)"""
    jira = Jira(
        url="http://52.169.187.138:8080/",
        username="admin",
        password="admin")

    return jira


def fetch_jira_projects():
    try:
        jira = get_jira()
        # Initializing all visible projects to jiraProjects variable
        jira_projects = jira.projects(included_archived=None)
        # print(jira_projects)
        return jira_projects

    except ValueError as projectError:
        print("Failed to fetch projects, Error: %", projectError)


def calculate_jira_projects(jira_projects):
    """"Returns all jira`s projects via list object."""
    # print(jira_projects)
    # First table project initialize
    list_of_projects = []
    for jira_project in jira_projects:
        # print(jira_project)
        line_of_project = (
            jira_project["id"],
            jira_project["key"],
            jira_project["name"],
            jira_project["projectTypeKey"])
        print(line_of_project)
        list_of_projects.append(line_of_project)

    return list_of_projects


def fetch_all_jira_issues():
    jira = get_jira()
    jira_issues = []
    jira_projects = fetch_jira_projects()
    # Initializing all visible projects to jiraProjects variable

    for jira_project in jira_projects:
        try:
            jira_project_issues = jira.get_all_project_issues(jira_project["id"], fields='*all')
        except ValueError as projectError:
            print("Failed to fetch "+jira_project["name"]+"'s issues, Error: %", projectError)
        for jira_project_issue in jira_project_issues:
            jira_issues.append(jira_project_issue)
    return jira_issues


def sort_all_jira_issues(jira_issues):
    sorted_json = {
        'Epic': [],
        'PortfolioEpic': [],
        'Initiative': [],
        'Sub-task': [],
        'Story+Task': [],
        'Test+Bug': []
    }
    for jira_issue in jira_issues:
        if jira_issue["fields"]["issuetype"]["name"] == "Epic":
            sorted_json['Epic'].append(jira_issue)
        if jira_issue["fields"]["issuetype"]["name"] == "PortfolioEpic":
            sorted_json['PortfolioEpic'].append(jira_issue)
        if jira_issue["fields"]["issuetype"]["name"] == "Initiative":
            sorted_json['Initiative'].append(jira_issue)
        if jira_issue["fields"]["issuetype"]["name"] == "Sub-task":
            sorted_json['Sub-task'].append(jira_issue)
        if jira_issue["fields"]["issuetype"]["name"] in ["Story", "Task"]:
            sorted_json['Story+Task'].append(jira_issue)
        if jira_issue["fields"]["issuetype"]["name"] in ["Test", "Bug"]:
            sorted_json['Test+Bug'].append(jira_issue)
    return sorted_json


def calculate_default_issue(issues):
    """"can calculate Portfolio-Epic,Initiative,Sub-Task,Epic"""
    list_of_calculated_issues = []
    for issue in issues:
        # pprint.pprint(issue)

        # checking if there is an assignee so we can get his name
        if issue["fields"]["assignee"] is not None:
            assignee_display_name = issue["fields"]["assignee"]["displayName"]  # Assignee
        else:
            assignee_display_name = None

        # checking if there is a parent issue
        if "parent" in issue["fields"]:
            parent_id = issue["fields"]["parent"]["fields"]["issuetype"]["id"]
        else:
            parent_id = None

        line_of_calculated_issue = (
            issue["key"],  # ID
            issue["fields"]["summary"],  # Name
            issue["fields"]["created"],  # CreationTime
            issue["fields"]["reporter"]["name"],  # ReporterID
            issue["fields"]["project"]["id"],  # ProjectID
            parent_id,  # ParentID
            issue["fields"]["status"]["statusCategory"]["name"],  # Status
            assignee_display_name)  # Assignee
        list_of_calculated_issues.append(line_of_calculated_issue)
        print(line_of_calculated_issue)

    return list_of_calculated_issues


def calculate_jira_story_issues(story_issues):
    """"Returns all jira`s Story via list object"""
    list_of_story_issues = []

    for story_issue in story_issues:
        # pprint.pprint(story_issue)

        # getting assignee data
        # checking if there is an assignee so we can get his name
        if story_issue["fields"]["assignee"] is not None:
            assignee_display_name = story_issue["fields"]["assignee"]["displayName"]  # Assignee
        else:
            assignee_display_name = None

        if story_issue["fields"]["description"] is not None:
            description = story_issue["fields"]["description"]  # Description
        else:
            description = None

        # getting sprintID data
        # (this all logic should be modified in the new api that will come out in 1 September 2020)
        if story_issue["fields"]["customfield_10104"] is not None:
            # running on all the sprints that connected to an issue
            for sprint_data in story_issue["fields"]["customfield_10104"]:

                # getting the Sprint State

                # getting the start and end index of where the state should be in the string
                start_state_index = sprint_data.find('state=') + 6
                end_state_index = sprint_data.find(',name=')

                # cutting the string to get the state
                sprint_state = sprint_data[start_state_index:end_state_index]

                # GET THE SPRINT ID
                # we want sprintID for only active sprints
                if (sprint_state == "ACTIVE"):
                    # getting the start and end index of where the id should be in the string
                    start_id_index = sprint_data.find('id=') + 3
                    end_id_index = sprint_data.find(',rapidViewId=')

                    # cutting the string to get the id
                    sprint_id = int(sprint_data[start_id_index:end_id_index])
        else:
            sprint_id = None

        # sorting data for db line
        line_of_story_issue = (

            story_issue["key"],  # StoryID
            story_issue["fields"]["summary"],  # Name
            story_issue["fields"]["created"],  # CreationTime
            story_issue["fields"]["reporter"]["name"],  # ReporterID
            story_issue["fields"]["project"]["id"],  # ProjectID
            None,  # EpicID
            sprint_id,  # SprintID
            story_issue["fields"]["status"]["statusCategory"]["name"],  # Status
            assignee_display_name,  # Assignee
            story_issue["fields"]["issuetype"]["name"],  # IssueType
            story_issue["fields"]["priority"]["name"],  # Priority
            description  # Description
        )
        list_of_story_issues.append(line_of_story_issue)
        print(line_of_story_issue)

    return list_of_story_issues


def calculate_jira_worklogs(issues):
    """"Returns all jira`s Work Log via list object"""

    list_of_worklogs = []

    for issue in issues:
        # fetching work logs only from Story and Task
        worklogs = issue["fields"]["worklog"]["worklogs"]
        # Check if there are work reports in that issue
        if (len(worklogs) != 0):
            # pprint.pprint(worklogs)
            for worklog in worklogs:
                story_id = None
                task_id = None
                # checking if its a Story or a Task for fields in DB table
                if issue["fields"]["issuetype"]["name"] == "Story":
                    story_id = issue["id"]
                elif issue["fields"]["issuetype"]["name"] == "Task":
                    task_id = issue["id"]
                # pprint.pprint(project_issue)
                # print(worklog)
                line_of_worklog = (
                    worklog["id"],  # worklogId
                    story_id,  # StoryID
                    task_id,  # TaskID
                    int((worklog["timeSpentSeconds"] / 60) / 60),  # Quantity
                    worklog["author"]["name"],  # ReporterID
                    worklog["created"]  # Date
                )
                list_of_worklogs.append(line_of_worklog)
                print(line_of_worklog)
    return list_of_worklogs


def fetch_jira_boards():
    """"Returns all jira`s boards Log via list object"""
    # fetching jira data
    jira = get_jira()
    # making the request
    try:
        request = requests.get(jira.url + "rest/agile/1.0/board",
                               auth=(jira.username, jira.password))
        # converting to json
        boards = request.json()
        return boards["values"]

    except ConnectionError as err:
        print("Failed to fetch boards: %", err, ', Error code : ', request.status_code)


def fetch_jira_sprints():
    # fetching jira data
    jira = get_jira()
    # fetching boards
    boards = fetch_jira_boards()
    # pprint.pprint(boards)
    sprints = []
    for board in boards:
        # making the request
        if board["type"] == "scrum":
            # pprint.pprint(board)
            try:
                request = requests.get(jira.url + "rest/agile/1.0/board/" + str(board["id"]) + "/sprint",
                                       auth=(jira.username, jira.password))
                # converting to json
                board_sprints = request.json()
                # pprint.pp(board_sprints)
            except ConnectionError as err:
                print("Failed to fetch sprint: %", err, ', Error code : ', request.status_code)
            # checking if the board has sprints
            if board_sprints["values"] is not None:
                for board_sprint in board_sprints["values"]:
                    sprints.append(board_sprint)
    return sprints


def calculate_jira_sprints(sprints):
    """"Returns all jira`s sprints Log via list object"""
    calculated_sprints = []
    # going over all sprints in the board and formating them to db
    for sprint in sprints:
        # checking if the spring is active or not
        is_active = False
        if sprint["state"] == "active":
           is_active = True
        # we are not fetching sprints that are in the future
        if sprint["state"] != "future":
            calculated_sprint=(
            sprint["id"],                     # SprintID
            sprint["name"],                   # Name
            sprint["startDate"],              # StartTime
            sprint["endDate"],                # EndTime
            None,                             # PiID
            is_active                         # is_active
            )

            calculated_sprints.append(calculated_sprint)
            print(calculated_sprint)
    return calculated_sprints


'''
getAllverstions()
The function return list of versions objects(tuolets) from all of the existing projects.
'''


def calculate_all_versions():
    list_of_all_versions = []
    jira = get_jira()
    # Initializing all visible projects to jiraProjects variable
    jira_projects = jira.projects(included_archived=None)
    try:
        for jira_project in jira_projects:
            # pp.pprint(jira_project)

            versions_per_project = jira.get_project_versions(jira_project["key"])
            for version in versions_per_project:
                v = (version["id"], #version_id
                     version["name"], #name
                     -1, #key
                     version["projectId"]
                     )
                list_of_all_versions.append(v)
        print(list_of_all_versions)
        return list_of_all_versions

    except ValueError as projectError:
        print("Failed to initialize issues, Error: %", projectError)


def main():
    #sending all info to postgresql
    '''

    sorted_issues = sort_all_jira_issues(fetch_all_jira_issues())
    # sending all info to postgresql
    dbm = DbMannager()
    dbm.postgressql_execute("jira_projects", calculate_jira_projects(fetch_jira_projects()))
    dbm.postgressql_execute("initiatives", calculate_default_issue(sorted_issues["Initiative"]))
    dbm.postgressql_execute("portfolio_epics", calculate_default_issue(sorted_issues["PortfolioEpic"]))
    dbm.postgressql_execute("epics", calculate_default_issue(sorted_issues["Epic"]))
    dbm.postgressql_execute("stories", calculate_jira_story_issues(sorted_issues["Story+Task"]))
    dbm.postgressql_execute("stories", calculate_jira_story_issues(sorted_issues["Test+Bug"]))
    dbm.postgressql_execute("sub_tasks", calculate_default_issue(sorted_issues["Sub-task"]))
    dbm.postgressql_execute("work_reports", calculate_jira_worklogs(sorted_issues["Story+Task"]))
    dbm.postgressql_execute("sprints", calculate_jira_sprints(fetch_jira_sprints()))
    dbm.postgressql_execute("versions", calculate_all_versions())
    dbm.close_connection()
    '''
    print('*' * 50,'<<< End of jira_exporter.py main() >>>','*' * 50)




    print('*' * 50, '<<< End of jira_exporter.py main() >>>', '*' * 50)

if __name__ == '__main__':
    main()
