import unittest
from random import randint
import zephyr_exporter
from json import *
import jira_exporter
import mock_resourses





'''
Test for the api side.
Tests related to the function  : get_cycles_by_project_id(main_json):
Require internet connectivity to run
'''
class ZephyrTests_zephyr_request_get_cycles(unittest.TestCase):

    def test_values(self):
        self.assertRaises(ValueError,zephyr_exporter.zephyr_request_get_cycles_by_project_id,-1)
        self.assertRaises(ValueError, zephyr_exporter.zephyr_request_get_cycles_by_project_id, "-123")
        self.assertRaises(ValueError, zephyr_exporter.zephyr_request_get_cycles_by_project_id, "abc123")
        self.assertRaises(ValueError, zephyr_exporter.zephyr_request_get_cycles_by_project_id, None)
        self.assertRaises(ValueError, zephyr_exporter.zephyr_request_get_cycles_by_project_id, 1234567899876543125)#projectId that isn't exsist


    def test_data_integrity_of_the_json_retrieved_for_the_zephyr_api_cycles(self):
        main_json = zephyr_exporter.zephyr_request_get_cycles_by_project_id(10103)['-1'][0]['1']
        self.assertEqual(10103, main_json['projectId'])
        self.assertNotEqual(None,main_json['name'])

    def test_get_all_cycles(self):
        all_cycles = zephyr_exporter.get_all_cycles()
        randon_index_all_cycles = randint(0, all_cycles.__len__() - 1)
        cycle = all_cycles[randon_index_all_cycles]
        #check data length :
        self.assertEqual(6,cycle.__len__())






'''
Test the manipulation of data of the function - get_cycles_by_project_id
Using mock the original data.
'''
class ZephyrTests_get_cycles_by_project_id_with_mock(unittest.TestCase):
    def test_attributes_of_elements_in_cycles_list(self):
        main_json = mock_resourses.get_original_main_json_from_zyphyr_api_get_cycles_by_project_id()
        cycles = zephyr_exporter.get_cycles_by_project_id(main_json)
        randon_index = randint(0, cycles.__len__() - 1)
        cycle = cycles[randon_index]
        #check that the critical attributes are not None
        self.assertNotEqual(cycle[3],None)#cycle[3] = project id
        self.assertNotEqual(cycle[0],None)#cycle[0] = cycle id

        # make sure the cycle object has 6 attributes
        self.assertEqual(cycle.__len__(),6)






'''

Tests related to the function  : get_url_for_zephyr_request_execute_search(list_of_projects_names):
'''
class ZepyrTest_get_url_for_zephyr_request_execute_search(unittest.TestCase):

    def test_values(self):
        self.assertRaises(ValueError,zephyr_exporter.get_url_for_zephyr_request_execute_search,None)
        self.assertRaises(ValueError, zephyr_exporter.get_url_for_zephyr_request_execute_search, [])



'''
Tests related to the function  : get_url_for_zephyr_request_execute_search(list_of_projects_names):
'''
class ZepyrTest_zephyr_request_get_all_execution_tests(unittest.TestCase):
    def test_values(self):
        self.assertRaises(ValueError,zephyr_exporter.zephyr_request_get_all_execution_tests,None)





'''
Test the api results.
Require internet connectivity to run
'''
class ZepyrTest_get_all_execution_tests(unittest.TestCase):
    def test_attributes_of_elements_in_list_of_executions(self):
        list_of_executions = zephyr_exporter.get_all_execution_tests(zephyr_exporter.zephyr_request_get_all_execution_tests(zephyr_exporter.get_url_for_zephyr_request_execute_search(zephyr_exporter.get_list_of_projects_names())))
        randon_index = randint(0, list_of_executions.__len__() - 1)
        execution = list_of_executions[randon_index]

        # check that the critical attributes are not None
        self.assertNotEqual(execution[0], None)  # execution_test[0] = executionId
        self.assertNotEqual(execution[2], None)  # execution_test[2] = test id
        self.assertNotEqual(execution[3], None)  # execution_test[3] = cycle id
        self.assertNotEqual(execution[2], None)  # execution_test[2] = test id

        # make sure the execution test object has 5 attributes
        self.assertEqual(execution.__len__(), 6)








'''
Test the manipulation of data of the function - get_all_execution_tests(main_json):
Using mock of the original data.
'''
class ZephyrTests_get_all_execution_tests_with_mock(unittest.TestCase):
    def test_attributes_of_elements_in_execution_tests(self):
        main_json = mock_resourses.get_main_json_zephyr_request_get_all_execution_tests()
        executions = zephyr_exporter.get_all_execution_tests(main_json)
        randon_index = randint(0, executions.__len__() - 1)
        execution_obj =  executions[randon_index]
        self.assertNotEqual(execution_obj[0], None)  # execution_test[0] = executionId
        self.assertNotEqual(execution_obj[2], None)  # execution_test[2] = test id
        self.assertNotEqual(execution_obj[3], None)  # execution_test[3] = cycle id
        self.assertNotEqual(execution_obj[2], None)  # execution_test[2] = test id






#run all of the tests from a specific list of test classes.
if __name__ == '__main__':
    print('Zephyr exporter UnitTest.')









