import unittest
from json import *
import jira_exporter


class CalculateJiraProjects(unittest.TestCase):
    # run test to verify  that get_jira_projects  isn't empty :
    jira_calculated_projects = jira_exporter.calculate_jira_projects(jira_exporter.fetch_jira_projects())
    mock = [{'id': '1234', 'key': "abcd", 'name': 'hello', 'projectTypeKey': "Scrum"},
            {'id': '5678', 'key': "efgh", 'name': 'world', 'projectTypeKey': "Kanban"}]
    calculated_mock = jira_exporter.calculate_jira_projects(mock)

    def test_projects_loop(self):
        self.assertEqual(len(self.calculated_mock), 2)
        self.assertEqual(len(jira_exporter.calculate_jira_projects([])), 0)

    def test_function_does_not_return_empty_list(self):
        self.assertNotEqual(self.jira_calculated_projects.__len__(), 0,
                            'The list that hold all jira projects is empty.')

    def test_project_id_is_int(self):
        for project_id in [project[0] for project in self.jira_calculated_projects]:
            # make partial validation to project id : make sure that the str that hold the it contain ony digits.
            self.assertEqual(all(char.isdigit() for char in project_id), True)
            '''
            This test is important for the function : get_url_for_zephyr_request_execute_search() - that
            return str query with project's id as parameters.
            '''

    def test_list_of_attributes_validation(self):
        for project in self.jira_calculated_projects:
            # check that the critical attributes are not None
            self.assertNotEqual(project[0], None)  # project id
            self.assertNotEqual(project[1], None)  # project key
            self.assertNotEqual(project[2], None)  # project name
            self.assertNotEqual(project[3], None)  # project projectTypeKey

    def test_list_of_attributes_validate_length(self):
        # make sure the project object has 4 attributes
        for project in self.jira_calculated_projects:
            self.assertEqual(project.__len__(), 4)


class CalculateDefaultIssue(unittest.TestCase):
    sorted_issues = jira_exporter.sort_all_jira_issues(jira_exporter.fetch_all_jira_issues())
    epic_issues = jira_exporter.calculate_default_issue(sorted_issues["Epic"])
    mock_issues = [{'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
  'fields': {'aggregateprogress': {'progress': 0, 'total': 0},
             'aggregatetimeestimate': None,
             'aggregatetimeoriginalestimate': None,
             'aggregatetimespent': None,
             'assignee': None,
             'attachment': [],
             'comment': {'comments': [],
                         'maxResults': 0,
                         'startAt': 0,
                         'total': 0},
             'components': [],
             'created': '2020-01-29T12:10:35.000+0000',
             'creator': {'active': True,
                         'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/useravatar?size=xsmall&avatarId=10347',
                                        '24x24': 'http://52.169.187.138:8080/secure/useravatar?size=small&avatarId=10347',
                                        '32x32': 'http://52.169.187.138:8080/secure/useravatar?size=medium&avatarId=10347',
                                        '48x48': 'http://52.169.187.138:8080/secure/useravatar?avatarId=10347'},
                         'displayName': 'mateMP',
                         'emailAddress': 'mate@devops.com',
                         'key': 'JIRAUSER10108',
                         'name': 'mateMP',
                         'self': 'http://52.169.187.138:8080/rest/api/2/user?username=mateMP',
                         'timeZone': 'GMT'},
             'customfield_10000': '{summaryBean=com.atlassian.jira.plugin.devstatus.rest.SummaryBean@1c8b9ff6[summary={pullrequest=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@1977640[overall=PullRequestOverallBean{stateCount=0, '
                                  "state='OPEN', "
                                  'details=PullRequestOverallDetails{openCount=0, '
                                  'mergedCount=0, '
                                  'declinedCount=0}},byInstanceType={}], '
                                  'build=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@1bcf812b[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BuildOverallBean@21d0d164[failedBuildCount=0,successfulBuildCount=0,unknownBuildCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'review=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@66a94181[overall=com.atlassian.jira.plugin.devstatus.summary.beans.ReviewsOverallBean@635b9360[stateCount=0,state=<null>,dueDate=<null>,overDue=false,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'deployment-environment=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@4fa80f9[overall=com.atlassian.jira.plugin.devstatus.summary.beans.DeploymentOverallBean@23062af3[topEnvironments=[],showProjects=false,successfulCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'repository=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@f0fd25b[overall=com.atlassian.jira.plugin.devstatus.summary.beans.CommitOverallBean@30708049[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'branch=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@605d23f6[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BranchOverallBean@729eccbc[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}]},errors=[],configErrors=[]], '
                                  'devSummaryJson={"cachedValue":{"errors":[],"configErrors":[],"summary":{"pullrequest":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":"OPEN","details":{"openCount":0,"mergedCount":0,"declinedCount":0,"total":0},"open":true},"byInstanceType":{}},"build":{"overall":{"count":0,"lastUpdated":null,"failedBuildCount":0,"successfulBuildCount":0,"unknownBuildCount":0},"byInstanceType":{}},"review":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":null,"dueDate":null,"overDue":false,"completed":false},"byInstanceType":{}},"deployment-environment":{"overall":{"count":0,"lastUpdated":null,"topEnvironments":[],"showProjects":false,"successfulCount":0},"byInstanceType":{}},"repository":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}},"branch":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}}}},"isStale":true}}',
             'customfield_10100': None,
             'customfield_10101': {'id': '10000',
                                   'self': 'http://52.169.187.138:8080/rest/api/2/customFieldOption/10000',
                                   'value': 'To Do'},
             'customfield_10102': 'Epic 2',
             'customfield_10103': 'ghx-label-14',
             'customfield_10104': None,
             'customfield_10105': '0|i000iv:',
             'customfield_10106': None,
             'customfield_10200': None,
             'customfield_10202': None,
             'customfield_10203': None,
             'customfield_10204': None,
             'customfield_10205': None,
             'customfield_10206': None,
             'customfield_10208': None,
             'customfield_10209': None,
             'customfield_10210': None,
             'customfield_10211': None,
             'customfield_10300': None,
             'customfield_10301': None,
             'customfield_10302': '2020-01-29',
             'customfield_10303': '2020-03-09',
             'customfield_10304': None,
             'customfield_10305': None,
             'customfield_10306': None,
             'customfield_10307': None,
             'customfield_10308': None,
             'description': None,
             'duedate': None,
             'environment': None,
             'fixVersions': [],
             'issuelinks': [],
             'issuetype': {'description': 'Created by Jira Software - do not '
                                          'edit or delete. Issue type for a '
                                          'big user story that needs to be '
                                          'broken down.',
                           'iconUrl': 'http://52.169.187.138:8080/images/icons/issuetypes/epic.svg',
                           'id': '10000',
                           'name': 'Epic',
                           'self': 'http://52.169.187.138:8080/rest/api/2/issuetype/10000',
                           'subtask': False},
             'labels': [],
             'lastViewed': None,
             'priority': {'iconUrl': 'http://52.169.187.138:8080/images/icons/priorities/medium.svg',
                          'id': '3',
                          'name': 'Medium',
                          'self': 'http://52.169.187.138:8080/rest/api/2/priority/3'},
             'progress': {'progress': 0, 'total': 0},
             'project': {'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/projectavatar?size=xsmall&avatarId=10324',
                                        '24x24': 'http://52.169.187.138:8080/secure/projectavatar?size=small&avatarId=10324',
                                        '32x32': 'http://52.169.187.138:8080/secure/projectavatar?size=medium&avatarId=10324',
                                        '48x48': 'http://52.169.187.138:8080/secure/projectavatar?avatarId=10324'},
                         'id': '10201',
                         'key': 'TJAP3',
                         'name': 'Test Jira Addon Project 3',
                         'projectTypeKey': 'software',
                         'self': 'http://52.169.187.138:8080/rest/api/2/project/10201'},
             'reporter': {'active': True,
                          'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/useravatar?size=xsmall&avatarId=10347',
                                         '24x24': 'http://52.169.187.138:8080/secure/useravatar?size=small&avatarId=10347',
                                         '32x32': 'http://52.169.187.138:8080/secure/useravatar?size=medium&avatarId=10347',
                                         '48x48': 'http://52.169.187.138:8080/secure/useravatar?avatarId=10347'},
                          'displayName': 'mateMP',
                          'emailAddress': 'mate@devops.com',
                          'key': 'JIRAUSER10108',
                          'name': 'mateMP',
                          'self': 'http://52.169.187.138:8080/rest/api/2/user?username=mateMP',
                          'timeZone': 'GMT'},
             'resolution': None,
             'resolutiondate': None,
             'status': {'description': '',
                        'iconUrl': 'http://52.169.187.138:8080/images/icons/statuses/generic.png',
                        'id': '10101',
                        'name': 'Open (Product Backlog)',
                        'self': 'http://52.169.187.138:8080/rest/api/2/status/10101',
                        'statusCategory': {'colorName': 'blue-gray',
                                           'id': 2,
                                           'key': 'new',
                                           'name': 'To Do',
                                           'self': 'http://52.169.187.138:8080/rest/api/2/statuscategory/2'}},
             'subtasks': [],
             'summary': 'Epic 2',
             'timeestimate': None,
             'timeoriginalestimate': None,
             'timespent': None,
             'timetracking': {},
             'updated': '2020-03-16T11:42:41.000+0000',
             'versions': [],
             'votes': {'hasVoted': False,
                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP3-6/votes',
                       'votes': 0},
             'watches': {'isWatching': False,
                         'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP3-6/watchers',
                         'watchCount': 1},
             'worklog': {'maxResults': 20,
                         'startAt': 0,
                         'total': 0,
                         'worklogs': []},
             'workratio': -1},
  'id': '10087',
  'key': 'TJAP3-6',
  'self': 'http://52.169.187.138:8080/rest/api/2/issue/10087'},
                       {'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
 'fields': {'aggregateprogress': {'percent': 0, 'progress': 0, 'total': 28800},
            'aggregatetimeestimate': 28800,
            'aggregatetimeoriginalestimate': 28800,
            'aggregatetimespent': None,
            'assignee': {'active': True,
                         'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/useravatar?size=xsmall&avatarId=10346',
                                        '24x24': 'http://52.169.187.138:8080/secure/useravatar?size=small&avatarId=10346',
                                        '32x32': 'http://52.169.187.138:8080/secure/useravatar?size=medium&avatarId=10346',
                                        '48x48': 'http://52.169.187.138:8080/secure/useravatar?avatarId=10346'},
                         'displayName': 'Alex Sh',
                         'emailAddress': 'Alexsh@iaf',
                         'key': 'JIRAUSER10200',
                         'name': 'Alexsh',
                         'self': 'http://52.169.187.138:8080/rest/api/2/user?username=Alexsh',
                         'timeZone': 'GMT'},
            'attachment': [],
            'comment': {'comments': [],
                        'maxResults': 0,
                        'startAt': 0,
                        'total': 0},
            'components': [],
            'created': '2020-01-29T12:45:31.000+0000',
            'creator': {'active': True,
                        'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/useravatar?size=xsmall&avatarId=10347',
                                       '24x24': 'http://52.169.187.138:8080/secure/useravatar?size=small&avatarId=10347',
                                       '32x32': 'http://52.169.187.138:8080/secure/useravatar?size=medium&avatarId=10347',
                                       '48x48': 'http://52.169.187.138:8080/secure/useravatar?avatarId=10347'},
                        'displayName': 'mateMP',
                        'emailAddress': 'mate@devops.com',
                        'key': 'JIRAUSER10108',
                        'name': 'mateMP',
                        'self': 'http://52.169.187.138:8080/rest/api/2/user?username=mateMP',
                        'timeZone': 'GMT'},
            'customfield_10000': '{summaryBean=com.atlassian.jira.plugin.devstatus.rest.SummaryBean@770a798[summary={pullrequest=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@1d465b87[overall=PullRequestOverallBean{stateCount=0, '
                                 "state='OPEN', "
                                 'details=PullRequestOverallDetails{openCount=0, '
                                 'mergedCount=0, '
                                 'declinedCount=0}},byInstanceType={}], '
                                 'build=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@7629e2ab[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BuildOverallBean@599c058a[failedBuildCount=0,successfulBuildCount=0,unknownBuildCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                 'review=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@50abfd6f[overall=com.atlassian.jira.plugin.devstatus.summary.beans.ReviewsOverallBean@1a38b935[stateCount=0,state=<null>,dueDate=<null>,overDue=false,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                 'deployment-environment=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@23bd2c63[overall=com.atlassian.jira.plugin.devstatus.summary.beans.DeploymentOverallBean@1a34cdcb[topEnvironments=[],showProjects=false,successfulCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                 'repository=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@15fb0b67[overall=com.atlassian.jira.plugin.devstatus.summary.beans.CommitOverallBean@4a55f9fc[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                 'branch=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@410ae1[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BranchOverallBean@3ca7c5b7[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}]},errors=[],configErrors=[]], '
                                 'devSummaryJson={"cachedValue":{"errors":[],"configErrors":[],"summary":{"pullrequest":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":"OPEN","details":{"openCount":0,"mergedCount":0,"declinedCount":0,"total":0},"open":true},"byInstanceType":{}},"build":{"overall":{"count":0,"lastUpdated":null,"failedBuildCount":0,"successfulBuildCount":0,"unknownBuildCount":0},"byInstanceType":{}},"review":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":null,"dueDate":null,"overDue":false,"completed":false},"byInstanceType":{}},"deployment-environment":{"overall":{"count":0,"lastUpdated":null,"topEnvironments":[],"showProjects":false,"successfulCount":0},"byInstanceType":{}},"repository":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}},"branch":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}}}},"isStale":true}}',
            'customfield_10100': None,
            'customfield_10104': ['com.atlassian.greenhopper.service.sprint.Sprint@1e7fbc68[id=5,rapidViewId=5,state=CLOSED,name=TJAB '
                                  'Sprint '
                                  '1,startDate=2020-02-02T07:13:03.439Z,endDate=2020-02-16T07:13:00.000Z,completeDate=2020-02-17T07:51:23.464Z,activatedDate=2020-02-02T07:13:03.439Z,sequence=5,goal=]',
                                  'com.atlassian.greenhopper.service.sprint.Sprint@4dc3f7d0[id=9,rapidViewId=8,state=CLOSED,name=TJAB '
                                  'Sprint '
                                  '2,startDate=2020-03-01T08:00:00.000Z,endDate=2020-03-12T17:00:00.000Z,completeDate=2020-03-01T12:57:42.411Z,activatedDate=2020-03-01T08:00:00.000Z,sequence=7,goal=]',
                                  'com.atlassian.greenhopper.service.sprint.Sprint@dfd5ec9[id=33,rapidViewId=35,state=CLOSED,name=TJAB '
                                  'Sprint '
                                  '1,startDate=2020-03-15T08:00:22.070Z,endDate=2020-03-19T17:00:00.000Z,completeDate=2020-03-29T11:06:00.458Z,activatedDate=2020-03-15T08:00:22.070Z,sequence=9,goal=]'],
            'customfield_10105': '0|i000ov:',
            'customfield_10200': None,
            'customfield_10202': None,
            'customfield_10203': None,
            'customfield_10204': None,
            'customfield_10205': None,
            'customfield_10206': None,
            'customfield_10208': None,
            'customfield_10209': None,
            'customfield_10210': None,
            'customfield_10211': None,
            'customfield_10300': None,
            'customfield_10301': None,
            'customfield_10302': '2020-01-29',
            'customfield_10303': '2020-01-29',
            'customfield_10304': None,
            'customfield_10305': None,
            'customfield_10306': None,
            'customfield_10307': None,
            'customfield_10308': None,
            'description': None,
            'duedate': None,
            'environment': None,
            'fixVersions': [],
            'issuelinks': [],
            'issuetype': {'avatarId': 10316,
                          'description': 'The sub-task of the issue',
                          'iconUrl': 'http://52.169.187.138:8080/secure/viewavatar?size=xsmall&avatarId=10316&avatarType=issuetype',
                          'id': '10003',
                          'name': 'Sub-task',
                          'self': 'http://52.169.187.138:8080/rest/api/2/issuetype/10003',
                          'subtask': True},
            'labels': [],
            'lastViewed': None,
            'parent': {'fields': {'issuetype': {'description': 'Created by '
                                                               'Jira Software '
                                                               '- do not edit '
                                                               'or delete. '
                                                               'Issue type for '
                                                               'a user story.',
                                                'iconUrl': 'http://52.169.187.138:8080/images/icons/issuetypes/story.svg',
                                                'id': '10001',
                                                'name': 'Story',
                                                'self': 'http://52.169.187.138:8080/rest/api/2/issuetype/10001',
                                                'subtask': False},
                                  'priority': {'iconUrl': 'http://52.169.187.138:8080/images/icons/priorities/medium.svg',
                                               'id': '3',
                                               'name': 'Medium',
                                               'self': 'http://52.169.187.138:8080/rest/api/2/priority/3'},
                                  'status': {'description': 'This issue is '
                                                            'being actively '
                                                            'worked on at the '
                                                            'moment by the '
                                                            'assignee.',
                                             'iconUrl': 'http://52.169.187.138:8080/images/icons/statuses/inprogress.png',
                                             'id': '3',
                                             'name': 'In Progress',
                                             'self': 'http://52.169.187.138:8080/rest/api/2/status/3',
                                             'statusCategory': {'colorName': 'yellow',
                                                                'id': 4,
                                                                'key': 'indeterminate',
                                                                'name': 'In '
                                                                        'Progress',
                                                                'self': 'http://52.169.187.138:8080/rest/api/2/statuscategory/4'}},
                                  'summary': 'Epic 1 Story 2'},
                       'id': '10092',
                       'key': 'TJAP3-105',
                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/10092'},
            'priority': {'iconUrl': 'http://52.169.187.138:8080/images/icons/priorities/medium.svg',
                         'id': '3',
                         'name': 'Medium',
                         'self': 'http://52.169.187.138:8080/rest/api/2/priority/3'},
            'progress': {'percent': 0, 'progress': 0, 'total': 28800},
            'project': {'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/projectavatar?size=xsmall&avatarId=10324',
                                       '24x24': 'http://52.169.187.138:8080/secure/projectavatar?size=small&avatarId=10324',
                                       '32x32': 'http://52.169.187.138:8080/secure/projectavatar?size=medium&avatarId=10324',
                                       '48x48': 'http://52.169.187.138:8080/secure/projectavatar?avatarId=10324'},
                        'id': '10201',
                        'key': 'TJAP3',
                        'name': 'Test Jira Addon Project 3',
                        'projectTypeKey': 'software',
                        'self': 'http://52.169.187.138:8080/rest/api/2/project/10201'},
            'reporter': {'active': True,
                         'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/useravatar?size=xsmall&avatarId=10347',
                                        '24x24': 'http://52.169.187.138:8080/secure/useravatar?size=small&avatarId=10347',
                                        '32x32': 'http://52.169.187.138:8080/secure/useravatar?size=medium&avatarId=10347',
                                        '48x48': 'http://52.169.187.138:8080/secure/useravatar?avatarId=10347'},
                         'displayName': 'mateMP',
                         'emailAddress': 'mate@devops.com',
                         'key': 'JIRAUSER10108',
                         'name': 'mateMP',
                         'self': 'http://52.169.187.138:8080/rest/api/2/user?username=mateMP',
                         'timeZone': 'GMT'},
            'resolution': None,
            'resolutiondate': None,
            'status': {'description': '',
                       'iconUrl': 'http://52.169.187.138:8080/',
                       'id': '10000',
                       'name': 'To Do (Sprint Backlog)',
                       'self': 'http://52.169.187.138:8080/rest/api/2/status/10000',
                       'statusCategory': {'colorName': 'blue-gray',
                                          'id': 2,
                                          'key': 'new',
                                          'name': 'To Do',
                                          'self': 'http://52.169.187.138:8080/rest/api/2/statuscategory/2'}},
            'subtasks': [],
            'summary': 'Epic 1 Story 2 Sub-Task 4',
            'timeestimate': 28800,
            'timeoriginalestimate': 28800,
            'timespent': None,
            'timetracking': {'originalEstimate': '1d',
                             'originalEstimateSeconds': 28800,
                             'remainingEstimate': '1d',
                             'remainingEstimateSeconds': 28800},
            'updated': '2020-03-16T11:43:36.000+0000',
            'versions': [],
            'votes': {'hasVoted': False,
                      'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP3-143/votes',
                      'votes': 0},
            'watches': {'isWatching': False,
                        'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP3-143/watchers',
                        'watchCount': 1},
            'worklog': {'maxResults': 20,
                        'startAt': 0,
                        'total': 0,
                        'worklogs': []},
            'workratio': 0},
 'id': '10114',
 'key': 'TJAP3-143',
 'self': 'http://52.169.187.138:8080/rest/api/2/issue/10114'}]
    calculated_mock_issues = jira_exporter.calculate_default_issue(mock_issues)

    # run test to verify that calculate_default_issues isn't empty :
    def test_function_does_not_return_empty_list(self):
        self.assertNotEqual(self.epic_issues.__len__(), 0,
                            'The list that hold all jira epic issues is empty.')

    # check that the critical attributes are not None
    def test_list_of_attributes_validate_not_none(self):
        for epic_issue in self.epic_issues:
            self.assertNotEqual(epic_issue[0], None)  # ID
            self.assertNotEqual(epic_issue[1], None)  # Name
            self.assertNotEqual(epic_issue[2], None)  # CreationTime
            self.assertNotEqual(epic_issue[3], None)  # ReporterID
            self.assertNotEqual(epic_issue[4], None)  # ProjectID
            self.assertNotEqual(epic_issue[6], None)  # Status

    def test_if_assignee(self):
        mock = self.calculated_mock_issues
        self.assertEqual(mock[0][7], None)
        self.assertNotEqual(mock[1][7], None)

    def test_if_parent(self):
        mock = self.calculated_mock_issues
        self.assertEqual(mock[0][5], None)
        self.assertNotEqual(mock[1][5], None)

    def test_for_loop(self):
        self.assertEqual(len(self.calculated_mock_issues), 2)
        self.assertEqual(len(jira_exporter.calculate_default_issue([])), 0)

    def test_list_of_attributes_validate_length(self):
        # make sure the project object has 8 attributes
        for epic_issue in self.epic_issues:
            self.assertEqual(epic_issue.__len__(), 8)


class CalculateJiraStoryIssues(unittest.TestCase):
    sorted_issues = jira_exporter.sort_all_jira_issues(jira_exporter.fetch_all_jira_issues())
    story_issues = jira_exporter.calculate_jira_story_issues(sorted_issues["Story+Task"])
    mock_story_issues = [{'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
  'fields': {'aggregateprogress': {'progress': 0, 'total': 0},
             'aggregatetimeestimate': None,
             'aggregatetimeoriginalestimate': None,
             'aggregatetimespent': None,
             'assignee': None,
             'attachment': [],
             'comment': {'comments': [],
                         'maxResults': 0,
                         'startAt': 0,
                         'total': 0},
             'components': [],
             'created': '2020-06-09T12:01:46.000+0000',
             'creator': {'active': True,
                         'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                        '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                        '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                        '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                         'displayName': 'admin',
                         'emailAddress': 'devops@devops.com',
                         'key': 'JIRAUSER10000',
                         'name': 'admin',
                         'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                         'timeZone': 'GMT'},
             'customfield_10000': '{summaryBean=com.atlassian.jira.plugin.devstatus.rest.SummaryBean@605b3327[summary={pullrequest=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@2300ca2a[overall=PullRequestOverallBean{stateCount=0, '
                                  "state='OPEN', "
                                  'details=PullRequestOverallDetails{openCount=0, '
                                  'mergedCount=0, '
                                  'declinedCount=0}},byInstanceType={}], '
                                  'build=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@28ff076a[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BuildOverallBean@d8bc9c5[failedBuildCount=0,successfulBuildCount=0,unknownBuildCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'review=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@ddf50c6[overall=com.atlassian.jira.plugin.devstatus.summary.beans.ReviewsOverallBean@3a8fdae7[stateCount=0,state=<null>,dueDate=<null>,overDue=false,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'deployment-environment=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@6a9b5e8b[overall=com.atlassian.jira.plugin.devstatus.summary.beans.DeploymentOverallBean@390d77fc[topEnvironments=[],showProjects=false,successfulCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'repository=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@4e23497b[overall=com.atlassian.jira.plugin.devstatus.summary.beans.CommitOverallBean@abffaba[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'branch=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@3a608260[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BranchOverallBean@6880a047[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}]},errors=[],configErrors=[]], '
                                  'devSummaryJson={"cachedValue":{"errors":[],"configErrors":[],"summary":{"pullrequest":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":"OPEN","details":{"openCount":0,"mergedCount":0,"declinedCount":0,"total":0},"open":true},"byInstanceType":{}},"build":{"overall":{"count":0,"lastUpdated":null,"failedBuildCount":0,"successfulBuildCount":0,"unknownBuildCount":0},"byInstanceType":{}},"review":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":null,"dueDate":null,"overDue":false,"completed":false},"byInstanceType":{}},"deployment-environment":{"overall":{"count":0,"lastUpdated":null,"topEnvironments":[],"showProjects":false,"successfulCount":0},"byInstanceType":{}},"repository":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}},"branch":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}}}},"isStale":false}}',
             'customfield_10100': None,
             'customfield_10104': None,
             'customfield_10105': '0|i001af:',
             'customfield_10106': None,
             'customfield_10200': None,
             'customfield_10202': None,
             'customfield_10203': None,
             'customfield_10204': None,
             'customfield_10205': None,
             'customfield_10206': None,
             'customfield_10208': None,
             'customfield_10209': None,
             'customfield_10210': None,
             'customfield_10211': None,
             'customfield_10300': None,
             'customfield_10301': None,
             'customfield_10302': None,
             'customfield_10303': None,
             'customfield_10304': None,
             'customfield_10305': None,
             'customfield_10306': None,
             'customfield_10307': None,
             'customfield_10308': None,
             'description': None,
             'duedate': None,
             'environment': None,
             'fixVersions': [],
             'issuelinks': [],
             'issuetype': {'description': 'Created by Jira Software - do not '
                                          'edit or delete. Issue type for a '
                                          'user story.',
                           'iconUrl': 'http://52.169.187.138:8080/images/icons/issuetypes/story.svg',
                           'id': '10001',
                           'name': 'Story',
                           'self': 'http://52.169.187.138:8080/rest/api/2/issuetype/10001',
                           'subtask': False},
             'labels': [],
             'lastViewed': '2020-06-09T14:36:40.300+0000',
             'priority': {'iconUrl': 'http://52.169.187.138:8080/images/icons/priorities/medium.svg',
                          'id': '3',
                          'name': 'Medium',
                          'self': 'http://52.169.187.138:8080/rest/api/2/priority/3'},
             'progress': {'progress': 0, 'total': 0},
             'project': {'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/projectavatar?size=xsmall&avatarId=10324',
                                        '24x24': 'http://52.169.187.138:8080/secure/projectavatar?size=small&avatarId=10324',
                                        '32x32': 'http://52.169.187.138:8080/secure/projectavatar?size=medium&avatarId=10324',
                                        '48x48': 'http://52.169.187.138:8080/secure/projectavatar?avatarId=10324'},
                         'id': '10103',
                         'key': 'TJAP2',
                         'name': 'ALM-Mock',
                         'projectTypeKey': 'software',
                         'self': 'http://52.169.187.138:8080/rest/api/2/project/10103'},
             'reporter': {'active': True,
                          'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                         '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                         '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                         '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                          'displayName': 'admin',
                          'emailAddress': 'devops@devops.com',
                          'key': 'JIRAUSER10000',
                          'name': 'admin',
                          'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                          'timeZone': 'GMT'},
             'resolution': None,
             'resolutiondate': None,
             'status': {'description': '',
                        'iconUrl': 'http://52.169.187.138:8080/',
                        'id': '10102',
                        'name': 'To Do',
                        'self': 'http://52.169.187.138:8080/rest/api/2/status/10102',
                        'statusCategory': {'colorName': 'blue-gray',
                                           'id': 2,
                                           'key': 'new',
                                           'name': 'To Do',
                                           'self': 'http://52.169.187.138:8080/rest/api/2/statuscategory/2'}},
             'subtasks': [],
             'summary': 'Story for unit-test',
             'timeestimate': None,
             'timeoriginalestimate': None,
             'timespent': None,
             'timetracking': {},
             'updated': '2020-06-09T14:36:40.000+0000',
             'versions': [],
             'votes': {'hasVoted': False,
                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-119/votes',
                       'votes': 0},
             'watches': {'isWatching': True,
                         'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-119/watchers',
                         'watchCount': 1},
             'worklog': {'maxResults': 20,
                         'startAt': 0,
                         'total': 0,
                         'worklogs': []},
             'workratio': -1},
  'id': '10600',
  'key': 'TJAP2-119',
  'self': 'http://52.169.187.138:8080/rest/api/2/issue/10600'},
                        {'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
                         'fields': {'aggregateprogress': {'percent': 0, 'progress': 0, 'total': 540},
                                    'aggregatetimeestimate': 540,
                                    'aggregatetimeoriginalestimate': 540,
                                    'aggregatetimespent': None,
                                    'assignee': {'active': True,
                                                 'avatarUrls': {
                                                     '16x16': 'http://52.169.187.138:8080/secure/useravatar?size=xsmall&avatarId=10347',
                                                     '24x24': 'http://52.169.187.138:8080/secure/useravatar?size=small&avatarId=10347',
                                                     '32x32': 'http://52.169.187.138:8080/secure/useravatar?size=medium&avatarId=10347',
                                                     '48x48': 'http://52.169.187.138:8080/secure/useravatar?avatarId=10347'},
                                                 'displayName': 'mateMP',
                                                 'emailAddress': 'mate@devops.com',
                                                 'key': 'JIRAUSER10108',
                                                 'name': 'mateMP',
                                                 'self': 'http://52.169.187.138:8080/rest/api/2/user?username=mateMP',
                                                 'timeZone': 'GMT'},
                                    'attachment': [],
                                    'comment': {'comments': [],
                                                'maxResults': 0,
                                                'startAt': 0,
                                                'total': 0},
                                    'components': [],
                                    'created': '2020-06-14T08:14:09.000+0000',
                                    'creator': {'active': True,
                                                'avatarUrls': {
                                                    '16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                    '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                    '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                    '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                'displayName': 'admin',
                                                'emailAddress': 'devops@devops.com',
                                                'key': 'JIRAUSER10000',
                                                'name': 'admin',
                                                'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                'timeZone': 'GMT'},
                                    'customfield_10000': '{summaryBean=com.atlassian.jira.plugin.devstatus.rest.SummaryBean@618a17c9[summary={pullrequest=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@927b66c[overall=PullRequestOverallBean{stateCount=0, '
                                                         "state='OPEN', "
                                                         'details=PullRequestOverallDetails{openCount=0, '
                                                         'mergedCount=0, '
                                                         'declinedCount=0}},byInstanceType={}], '
                                                         'build=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@2bf39104[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BuildOverallBean@1f53c6c0[failedBuildCount=0,successfulBuildCount=0,unknownBuildCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                                         'review=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@11d9be48[overall=com.atlassian.jira.plugin.devstatus.summary.beans.ReviewsOverallBean@3ccc0db6[stateCount=0,state=<null>,dueDate=<null>,overDue=false,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                                         'deployment-environment=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@b80c09a[overall=com.atlassian.jira.plugin.devstatus.summary.beans.DeploymentOverallBean@ae462ec[topEnvironments=[],showProjects=false,successfulCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                                         'repository=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@5693b606[overall=com.atlassian.jira.plugin.devstatus.summary.beans.CommitOverallBean@a6ea0df[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                                         'branch=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@7e73518[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BranchOverallBean@2e3dc40a[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}]},errors=[],configErrors=[]], '
                                                         'devSummaryJson={"cachedValue":{"errors":[],"configErrors":[],"summary":{"pullrequest":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":"OPEN","details":{"openCount":0,"mergedCount":0,"declinedCount":0,"total":0},"open":true},"byInstanceType":{}},"build":{"overall":{"count":0,"lastUpdated":null,"failedBuildCount":0,"successfulBuildCount":0,"unknownBuildCount":0},"byInstanceType":{}},"review":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":null,"dueDate":null,"overDue":false,"completed":false},"byInstanceType":{}},"deployment-environment":{"overall":{"count":0,"lastUpdated":null,"topEnvironments":[],"showProjects":false,"successfulCount":0},"byInstanceType":{}},"repository":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}},"branch":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}}}},"isStale":true}}',
                                    'customfield_10100': 'TJAP2-125',
                                    'customfield_10104': [
                                        'com.atlassian.greenhopper.service.sprint.Sprint@2aa6b5d4[id=37,rapidViewId=39,state=ACTIVE,name=hey '
                                        "its guy's sprint "
                                        'now,startDate=2020-06-01T09:42:00.000Z,endDate=2020-06-15T18:42:00.000Z,completeDate=<null>,activatedDate=2020-06-01T09:42:20.062Z,sequence=70,goal=]'],
                                    'customfield_10105': '0|i001b0:',
                                    'customfield_10106': None,
                                    'customfield_10200': None,
                                    'customfield_10202': None,
                                    'customfield_10203': None,
                                    'customfield_10204': None,
                                    'customfield_10205': None,
                                    'customfield_10206': None,
                                    'customfield_10208': None,
                                    'customfield_10209': None,
                                    'customfield_10210': None,
                                    'customfield_10211': None,
                                    'customfield_10300': None,
                                    'customfield_10301': None,
                                    'customfield_10302': None,
                                    'customfield_10303': None,
                                    'customfield_10304': None,
                                    'customfield_10305': None,
                                    'customfield_10306': None,
                                    'customfield_10307': None,
                                    'customfield_10308': None,
                                    'description': 'well, who does not love cookies ?\xa0:D',
                                    'duedate': None,
                                    'environment': None,
                                    'fixVersions': [],
                                    'issuelinks': [],
                                    'issuetype': {'description': 'Created by Jira Software - do not '
                                                                 'edit or delete. Issue type for a '
                                                                 'user story.',
                                                  'iconUrl': 'http://52.169.187.138:8080/images/icons/issuetypes/story.svg',
                                                  'id': '10001',
                                                  'name': 'Story',
                                                  'self': 'http://52.169.187.138:8080/rest/api/2/issuetype/10001',
                                                  'subtask': False},
                                    'labels': [],
                                    'lastViewed': '2020-06-14T08:14:10.024+0000',
                                    'priority': {
                                        'iconUrl': 'http://52.169.187.138:8080/images/icons/priorities/medium.svg',
                                        'id': '3',
                                        'name': 'Medium',
                                        'self': 'http://52.169.187.138:8080/rest/api/2/priority/3'},
                                    'progress': {'percent': 0, 'progress': 0, 'total': 540},
                                    'project': {'avatarUrls': {
                                        '16x16': 'http://52.169.187.138:8080/secure/projectavatar?size=xsmall&avatarId=10324',
                                        '24x24': 'http://52.169.187.138:8080/secure/projectavatar?size=small&avatarId=10324',
                                        '32x32': 'http://52.169.187.138:8080/secure/projectavatar?size=medium&avatarId=10324',
                                        '48x48': 'http://52.169.187.138:8080/secure/projectavatar?avatarId=10324'},
                                                'id': '10103',
                                                'key': 'TJAP2',
                                                'name': 'ALM-Mock',
                                                'projectTypeKey': 'software',
                                                'self': 'http://52.169.187.138:8080/rest/api/2/project/10103'},
                                    'reporter': {'active': True,
                                                 'avatarUrls': {
                                                     '16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                     '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                     '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                     '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                 'displayName': 'admin',
                                                 'emailAddress': 'devops@devops.com',
                                                 'key': 'JIRAUSER10000',
                                                 'name': 'admin',
                                                 'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                 'timeZone': 'GMT'},
                                    'resolution': None,
                                    'resolutiondate': None,
                                    'status': {'description': '',
                                               'iconUrl': 'http://52.169.187.138:8080/',
                                               'id': '10102',
                                               'name': 'To Do',
                                               'self': 'http://52.169.187.138:8080/rest/api/2/status/10102',
                                               'statusCategory': {'colorName': 'blue-gray',
                                                                  'id': 2,
                                                                  'key': 'new',
                                                                  'name': 'To Do',
                                                                  'self': 'http://52.169.187.138:8080/rest/api/2/statuscategory/2'}},
                                    'subtasks': [],
                                    'summary': 'cookie monster',
                                    'timeestimate': 540,
                                    'timeoriginalestimate': 540,
                                    'timespent': None,
                                    'timetracking': {'originalEstimate': '9m',
                                                     'originalEstimateSeconds': 540,
                                                     'remainingEstimate': '9m',
                                                     'remainingEstimateSeconds': 540},
                                    'updated': '2020-06-14T08:14:10.000+0000',
                                    'versions': [],
                                    'votes': {'hasVoted': False,
                                              'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-126/votes',
                                              'votes': 0},
                                    'watches': {'isWatching': True,
                                                'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-126/watchers',
                                                'watchCount': 1},
                                    'worklog': {'maxResults': 20,
                                                'startAt': 0,
                                                'total': 0,
                                                'worklogs': []},
                                    'workratio': 0},
                         'id': '10801',
                         'key': 'TJAP2-126',
                         'self': 'http://52.169.187.138:8080/rest/api/2/issue/10801'}]
    calculated_mock_story_issues = jira_exporter.calculate_jira_story_issues(mock_story_issues)

    # run test to verify that calculate_story_issues isn't empty :
    def test_function_does_not_return_empty_list(self):
        self.assertNotEqual(self.story_issues.__len__(), 0,
                            'The list that hold all jira story type issues is empty.')

    # check that the critical attributes are not None
    def test_list_of_attributes_validation(self):
        for story_issue in self.story_issues:
            self.assertNotEqual(story_issue[0], None)  # StoryID
            self.assertNotEqual(story_issue[1], None)  # Name
            self.assertNotEqual(story_issue[2], None)  # CreationTime
            self.assertNotEqual(story_issue[3], None)  # ReporterID
            self.assertNotEqual(story_issue[4], None)  # ProjectID
            self.assertNotEqual(story_issue[7], None)  # Status
            self.assertNotEqual(story_issue[9], None)  # IssueType
            self.assertNotEqual(story_issue[10], None)  # Priority

    def test_assignee_validation(self):
        mock = self.calculated_mock_story_issues
        self.assertEqual(mock[0][8], None)  # Assignee
        self.assertNotEqual(mock[1][8], None)  # Assignee

    def test_description_validation(self):
        mock = self.calculated_mock_story_issues
        self.assertEqual(mock[0][11], None)  # Description
        self.assertNotEqual(mock[1][11], None)  # Description

    def test_sprint_id_validation(self):
        mock = self.calculated_mock_story_issues
        self.assertEqual(mock[0][6], None)  # SprintID
        self.assertNotEqual(mock[1][6], None)  # SprintID

    def test_for_loop(self):
        mock = self.calculated_mock_story_issues
        self.assertEqual(len(mock), 2)  # EpicID
        self.assertEqual(len(jira_exporter.calculate_jira_story_issues([])), 0)

    def test_list_of_attributes_validate_length(self):
        # make sure the project object has 12 attributes
        for story_issue in self.story_issues:
            self.assertEqual(story_issue.__len__(), 12)


class CalculateJiraWorklogs(unittest.TestCase):
    sorted_issues = jira_exporter.sort_all_jira_issues(jira_exporter.fetch_all_jira_issues())
    worklogs = jira_exporter.calculate_jira_worklogs(sorted_issues["Story+Task"])
    no_worklogs_mock = [{'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
  'fields': {'aggregateprogress': {'progress': 0, 'total': 0},
             'aggregatetimeestimate': None,
             'aggregatetimeoriginalestimate': None,
             'aggregatetimespent': None,
             'assignee': None,
             'attachment': [],
             'comment': {'comments': [],
                         'maxResults': 0,
                         'startAt': 0,
                         'total': 0},
             'components': [],
             'created': '2020-06-09T12:01:46.000+0000',
             'creator': {'active': True,
                         'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                        '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                        '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                        '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                         'displayName': 'admin',
                         'emailAddress': 'devops@devops.com',
                         'key': 'JIRAUSER10000',
                         'name': 'admin',
                         'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                         'timeZone': 'GMT'},
             'customfield_10000': '{summaryBean=com.atlassian.jira.plugin.devstatus.rest.SummaryBean@605b3327[summary={pullrequest=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@2300ca2a[overall=PullRequestOverallBean{stateCount=0, '
                                  "state='OPEN', "
                                  'details=PullRequestOverallDetails{openCount=0, '
                                  'mergedCount=0, '
                                  'declinedCount=0}},byInstanceType={}], '
                                  'build=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@28ff076a[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BuildOverallBean@d8bc9c5[failedBuildCount=0,successfulBuildCount=0,unknownBuildCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'review=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@ddf50c6[overall=com.atlassian.jira.plugin.devstatus.summary.beans.ReviewsOverallBean@3a8fdae7[stateCount=0,state=<null>,dueDate=<null>,overDue=false,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'deployment-environment=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@6a9b5e8b[overall=com.atlassian.jira.plugin.devstatus.summary.beans.DeploymentOverallBean@390d77fc[topEnvironments=[],showProjects=false,successfulCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'repository=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@4e23497b[overall=com.atlassian.jira.plugin.devstatus.summary.beans.CommitOverallBean@abffaba[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'branch=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@3a608260[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BranchOverallBean@6880a047[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}]},errors=[],configErrors=[]], '
                                  'devSummaryJson={"cachedValue":{"errors":[],"configErrors":[],"summary":{"pullrequest":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":"OPEN","details":{"openCount":0,"mergedCount":0,"declinedCount":0,"total":0},"open":true},"byInstanceType":{}},"build":{"overall":{"count":0,"lastUpdated":null,"failedBuildCount":0,"successfulBuildCount":0,"unknownBuildCount":0},"byInstanceType":{}},"review":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":null,"dueDate":null,"overDue":false,"completed":false},"byInstanceType":{}},"deployment-environment":{"overall":{"count":0,"lastUpdated":null,"topEnvironments":[],"showProjects":false,"successfulCount":0},"byInstanceType":{}},"repository":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}},"branch":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}}}},"isStale":false}}',
             'customfield_10100': None,
             'customfield_10104': None,
             'customfield_10105': '0|i001af:',
             'customfield_10106': None,
             'customfield_10200': None,
             'customfield_10202': None,
             'customfield_10203': None,
             'customfield_10204': None,
             'customfield_10205': None,
             'customfield_10206': None,
             'customfield_10208': None,
             'customfield_10209': None,
             'customfield_10210': None,
             'customfield_10211': None,
             'customfield_10300': None,
             'customfield_10301': None,
             'customfield_10302': None,
             'customfield_10303': None,
             'customfield_10304': None,
             'customfield_10305': None,
             'customfield_10306': None,
             'customfield_10307': None,
             'customfield_10308': None,
             'description': None,
             'duedate': None,
             'environment': None,
             'fixVersions': [],
             'issuelinks': [],
             'issuetype': {'description': 'Created by Jira Software - do not '
                                          'edit or delete. Issue type for a '
                                          'user story.',
                           'iconUrl': 'http://52.169.187.138:8080/images/icons/issuetypes/story.svg',
                           'id': '10001',
                           'name': 'Story',
                           'self': 'http://52.169.187.138:8080/rest/api/2/issuetype/10001',
                           'subtask': False},
             'labels': [],
             'lastViewed': '2020-06-09T14:36:40.300+0000',
             'priority': {'iconUrl': 'http://52.169.187.138:8080/images/icons/priorities/medium.svg',
                          'id': '3',
                          'name': 'Medium',
                          'self': 'http://52.169.187.138:8080/rest/api/2/priority/3'},
             'progress': {'progress': 0, 'total': 0},
             'project': {'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/projectavatar?size=xsmall&avatarId=10324',
                                        '24x24': 'http://52.169.187.138:8080/secure/projectavatar?size=small&avatarId=10324',
                                        '32x32': 'http://52.169.187.138:8080/secure/projectavatar?size=medium&avatarId=10324',
                                        '48x48': 'http://52.169.187.138:8080/secure/projectavatar?avatarId=10324'},
                         'id': '10103',
                         'key': 'TJAP2',
                         'name': 'ALM-Mock',
                         'projectTypeKey': 'software',
                         'self': 'http://52.169.187.138:8080/rest/api/2/project/10103'},
             'reporter': {'active': True,
                          'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                         '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                         '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                         '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                          'displayName': 'admin',
                          'emailAddress': 'devops@devops.com',
                          'key': 'JIRAUSER10000',
                          'name': 'admin',
                          'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                          'timeZone': 'GMT'},
             'resolution': None,
             'resolutiondate': None,
             'status': {'description': '',
                        'iconUrl': 'http://52.169.187.138:8080/',
                        'id': '10102',
                        'name': 'To Do',
                        'self': 'http://52.169.187.138:8080/rest/api/2/status/10102',
                        'statusCategory': {'colorName': 'blue-gray',
                                           'id': 2,
                                           'key': 'new',
                                           'name': 'To Do',
                                           'self': 'http://52.169.187.138:8080/rest/api/2/statuscategory/2'}},
             'subtasks': [],
             'summary': 'Story for unit-test',
             'timeestimate': None,
             'timeoriginalestimate': None,
             'timespent': None,
             'timetracking': {},
             'updated': '2020-06-09T14:36:40.000+0000',
             'versions': [],
             'votes': {'hasVoted': False,
                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-119/votes',
                       'votes': 0},
             'watches': {'isWatching': True,
                         'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-119/watchers',
                         'watchCount': 1},
             'worklog': {'maxResults': 20,
                         'startAt': 0,
                         'total': 0,
                         'worklogs': []},
             'workratio': -1},
  'id': '10600',
  'key': 'TJAP2-119',
  'self': 'http://52.169.187.138:8080/rest/api/2/issue/10600'}]
    worklogs_mock_for_story = {'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
  'fields': {'aggregateprogress': {'percent': 100,
                                   'progress': 97200,
                                   'total': 97200},
             'aggregatetimeestimate': None,
             'aggregatetimeoriginalestimate': None,
             'aggregatetimespent': 97200,
             'assignee': {'active': True,
                          'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/useravatar?size=xsmall&avatarId=10347',
                                         '24x24': 'http://52.169.187.138:8080/secure/useravatar?size=small&avatarId=10347',
                                         '32x32': 'http://52.169.187.138:8080/secure/useravatar?size=medium&avatarId=10347',
                                         '48x48': 'http://52.169.187.138:8080/secure/useravatar?avatarId=10347'},
                          'displayName': 'mateMP',
                          'emailAddress': 'mate@devops.com',
                          'key': 'JIRAUSER10108',
                          'name': 'mateMP',
                          'self': 'http://52.169.187.138:8080/rest/api/2/user?username=mateMP',
                          'timeZone': 'GMT'},
             'attachment': [],
             'comment': {'comments': [],
                         'maxResults': 0,
                         'startAt': 0,
                         'total': 0},
             'components': [],
             'created': '2020-06-10T08:52:53.000+0000',
             'creator': {'active': True,
                         'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                        '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                        '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                        '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                         'displayName': 'admin',
                         'emailAddress': 'devops@devops.com',
                         'key': 'JIRAUSER10000',
                         'name': 'admin',
                         'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                         'timeZone': 'GMT'},
             'customfield_10000': '{summaryBean=com.atlassian.jira.plugin.devstatus.rest.SummaryBean@546d88bf[summary={pullrequest=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@f4b7829[overall=PullRequestOverallBean{stateCount=0, '
                                  "state='OPEN', "
                                  'details=PullRequestOverallDetails{openCount=0, '
                                  'mergedCount=0, '
                                  'declinedCount=0}},byInstanceType={}], '
                                  'build=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@72cae5c8[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BuildOverallBean@26144188[failedBuildCount=0,successfulBuildCount=0,unknownBuildCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'review=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@389e3018[overall=com.atlassian.jira.plugin.devstatus.summary.beans.ReviewsOverallBean@27f90b93[stateCount=0,state=<null>,dueDate=<null>,overDue=false,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'deployment-environment=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@2b84e01[overall=com.atlassian.jira.plugin.devstatus.summary.beans.DeploymentOverallBean@6a447df[topEnvironments=[],showProjects=false,successfulCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'repository=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@4ff3b9e3[overall=com.atlassian.jira.plugin.devstatus.summary.beans.CommitOverallBean@20b9961d[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'branch=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@60311c4d[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BranchOverallBean@440f352b[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}]},errors=[],configErrors=[]], '
                                  'devSummaryJson={"cachedValue":{"errors":[],"configErrors":[],"summary":{"pullrequest":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":"OPEN","details":{"openCount":0,"mergedCount":0,"declinedCount":0,"total":0},"open":true},"byInstanceType":{}},"build":{"overall":{"count":0,"lastUpdated":null,"failedBuildCount":0,"successfulBuildCount":0,"unknownBuildCount":0},"byInstanceType":{}},"review":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":null,"dueDate":null,"overDue":false,"completed":false},"byInstanceType":{}},"deployment-environment":{"overall":{"count":0,"lastUpdated":null,"topEnvironments":[],"showProjects":false,"successfulCount":0},"byInstanceType":{}},"repository":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}},"branch":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}}}},"isStale":false}}',
             'customfield_10100': None,
             'customfield_10104': None,
             'customfield_10105': '0|i001av:',
             'customfield_10106': None,
             'customfield_10200': None,
             'customfield_10202': None,
             'customfield_10203': None,
             'customfield_10204': None,
             'customfield_10205': None,
             'customfield_10206': None,
             'customfield_10208': None,
             'customfield_10209': None,
             'customfield_10210': None,
             'customfield_10211': None,
             'customfield_10300': None,
             'customfield_10301': None,
             'customfield_10302': None,
             'customfield_10303': None,
             'customfield_10304': None,
             'customfield_10305': None,
             'customfield_10306': None,
             'customfield_10307': None,
             'customfield_10308': None,
             'description': None,
             'duedate': None,
             'environment': None,
             'fixVersions': [],
             'issuelinks': [],
             'issuetype': {'description': 'Created by Jira Software - do not '
                                          'edit or delete. Issue type for a '
                                          'user story.',
                           'iconUrl': 'http://52.169.187.138:8080/images/icons/issuetypes/story.svg',
                           'id': '10001',
                           'name': 'Story',
                           'self': 'http://52.169.187.138:8080/rest/api/2/issuetype/10001',
                           'subtask': False},
             'labels': [],
             'lastViewed': '2020-06-10T08:52:53.575+0000',
             'priority': {'iconUrl': 'http://52.169.187.138:8080/images/icons/priorities/medium.svg',
                          'id': '3',
                          'name': 'Medium',
                          'self': 'http://52.169.187.138:8080/rest/api/2/priority/3'},
             'progress': {'percent': 100, 'progress': 97200, 'total': 97200},
             'project': {'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/projectavatar?size=xsmall&avatarId=10324',
                                        '24x24': 'http://52.169.187.138:8080/secure/projectavatar?size=small&avatarId=10324',
                                        '32x32': 'http://52.169.187.138:8080/secure/projectavatar?size=medium&avatarId=10324',
                                        '48x48': 'http://52.169.187.138:8080/secure/projectavatar?avatarId=10324'},
                         'id': '10103',
                         'key': 'TJAP2',
                         'name': 'ALM-Mock',
                         'projectTypeKey': 'software',
                         'self': 'http://52.169.187.138:8080/rest/api/2/project/10103'},
             'reporter': {'active': True,
                          'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                         '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                         '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                         '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                          'displayName': 'admin',
                          'emailAddress': 'devops@devops.com',
                          'key': 'JIRAUSER10000',
                          'name': 'admin',
                          'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                          'timeZone': 'GMT'},
             'resolution': None,
             'resolutiondate': None,
             'status': {'description': '',
                        'iconUrl': 'http://52.169.187.138:8080/',
                        'id': '10102',
                        'name': 'To Do',
                        'self': 'http://52.169.187.138:8080/rest/api/2/status/10102',
                        'statusCategory': {'colorName': 'blue-gray',
                                           'id': 2,
                                           'key': 'new',
                                           'name': 'To Do',
                                           'self': 'http://52.169.187.138:8080/rest/api/2/statuscategory/2'}},
             'subtasks': [],
             'summary': 'reported story',
             'timeestimate': None,
             'timeoriginalestimate': None,
             'timespent': 97200,
             'timetracking': {'timeSpent': '3d 3h', 'timeSpentSeconds': 97200},
             'updated': '2020-06-10T08:54:02.000+0000',
             'versions': [],
             'votes': {'hasVoted': False,
                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-121/votes',
                       'votes': 0},
             'watches': {'isWatching': True,
                         'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-121/watchers',
                         'watchCount': 1},
             'worklog': {'maxResults': 20,
                         'startAt': 0,
                         'total': 3,
                         'worklogs': [{'author': {'active': True,
                                                  'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                 '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                 '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                 '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                  'displayName': 'admin',
                                                  'emailAddress': 'devops@devops.com',
                                                  'key': 'JIRAUSER10000',
                                                  'name': 'admin',
                                                  'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                  'timeZone': 'GMT'},
                                       'comment': 'Working on issue TJAP2-121',
                                       'created': '2020-06-10T08:53:53.679+0000',
                                       'id': '10303',
                                       'issueId': '10701',
                                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/10701/worklog/10303',
                                       'started': '2020-06-10T00:00:00.000+0000',
                                       'timeSpent': '1d 1h',
                                       'timeSpentSeconds': 32400,
                                       'updateAuthor': {'active': True,
                                                        'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                       '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                       '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                       '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                        'displayName': 'admin',
                                                        'emailAddress': 'devops@devops.com',
                                                        'key': 'JIRAUSER10000',
                                                        'name': 'admin',
                                                        'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                        'timeZone': 'GMT'},
                                       'updated': '2020-06-10T08:53:53.679+0000'},
                                      {'author': {'active': True,
                                                  'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                 '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                 '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                 '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                  'displayName': 'admin',
                                                  'emailAddress': 'devops@devops.com',
                                                  'key': 'JIRAUSER10000',
                                                  'name': 'admin',
                                                  'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                  'timeZone': 'GMT'},
                                       'comment': 'Working on issue TJAP2-121',
                                       'created': '2020-06-10T08:53:57.748+0000',
                                       'id': '10304',
                                       'issueId': '10701',
                                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/10701/worklog/10304',
                                       'started': '2020-06-09T00:00:00.000+0000',
                                       'timeSpent': '1d 1h',
                                       'timeSpentSeconds': 32400,
                                       'updateAuthor': {'active': True,
                                                        'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                       '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                       '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                       '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                        'displayName': 'admin',
                                                        'emailAddress': 'devops@devops.com',
                                                        'key': 'JIRAUSER10000',
                                                        'name': 'admin',
                                                        'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                        'timeZone': 'GMT'},
                                       'updated': '2020-06-10T08:53:57.748+0000'},
                                      {'author': {'active': True,
                                                  'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                 '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                 '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                 '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                  'displayName': 'admin',
                                                  'emailAddress': 'devops@devops.com',
                                                  'key': 'JIRAUSER10000',
                                                  'name': 'admin',
                                                  'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                  'timeZone': 'GMT'},
                                       'comment': 'Working on issue TJAP2-121',
                                       'created': '2020-06-10T08:54:02.892+0000',
                                       'id': '10305',
                                       'issueId': '10701',
                                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/10701/worklog/10305',
                                       'started': '2020-06-08T00:00:00.000+0000',
                                       'timeSpent': '1d 1h',
                                       'timeSpentSeconds': 32400,
                                       'updateAuthor': {'active': True,
                                                        'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                       '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                       '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                       '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                        'displayName': 'admin',
                                                        'emailAddress': 'devops@devops.com',
                                                        'key': 'JIRAUSER10000',
                                                        'name': 'admin',
                                                        'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                        'timeZone': 'GMT'},
                                       'updated': '2020-06-10T08:54:02.892+0000'}]},
             'workratio': -1},
  'id': '10701',
  'key': 'TJAP2-121',
  'self': 'http://52.169.187.138:8080/rest/api/2/issue/10701'}
    worklogs_mock_for_task = {'expand': 'operations,versionedRepresentations,editmeta,changelog,renderedFields',
  'fields': {'aggregateprogress': {'percent': 100,
                                   'progress': 97200,
                                   'total': 97200},
             'aggregatetimeestimate': None,
             'aggregatetimeoriginalestimate': None,
             'aggregatetimespent': 97200,
             'assignee': {'active': True,
                          'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/useravatar?size=xsmall&avatarId=10347',
                                         '24x24': 'http://52.169.187.138:8080/secure/useravatar?size=small&avatarId=10347',
                                         '32x32': 'http://52.169.187.138:8080/secure/useravatar?size=medium&avatarId=10347',
                                         '48x48': 'http://52.169.187.138:8080/secure/useravatar?avatarId=10347'},
                          'displayName': 'mateMP',
                          'emailAddress': 'mate@devops.com',
                          'key': 'JIRAUSER10108',
                          'name': 'mateMP',
                          'self': 'http://52.169.187.138:8080/rest/api/2/user?username=mateMP',
                          'timeZone': 'GMT'},
             'attachment': [],
             'comment': {'comments': [],
                         'maxResults': 0,
                         'startAt': 0,
                         'total': 0},
             'components': [],
             'created': '2020-06-10T08:52:40.000+0000',
             'creator': {'active': True,
                         'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                        '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                        '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                        '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                         'displayName': 'admin',
                         'emailAddress': 'devops@devops.com',
                         'key': 'JIRAUSER10000',
                         'name': 'admin',
                         'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                         'timeZone': 'GMT'},
             'customfield_10000': '{summaryBean=com.atlassian.jira.plugin.devstatus.rest.SummaryBean@1e664e66[summary={pullrequest=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@4d108b5a[overall=PullRequestOverallBean{stateCount=0, '
                                  "state='OPEN', "
                                  'details=PullRequestOverallDetails{openCount=0, '
                                  'mergedCount=0, '
                                  'declinedCount=0}},byInstanceType={}], '
                                  'build=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@2fa8b2ae[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BuildOverallBean@66aba65a[failedBuildCount=0,successfulBuildCount=0,unknownBuildCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'review=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@f968fec[overall=com.atlassian.jira.plugin.devstatus.summary.beans.ReviewsOverallBean@2442823[stateCount=0,state=<null>,dueDate=<null>,overDue=false,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'deployment-environment=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@6813c707[overall=com.atlassian.jira.plugin.devstatus.summary.beans.DeploymentOverallBean@53b13529[topEnvironments=[],showProjects=false,successfulCount=0,count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'repository=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@68e3295c[overall=com.atlassian.jira.plugin.devstatus.summary.beans.CommitOverallBean@4b451153[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}], '
                                  'branch=com.atlassian.jira.plugin.devstatus.rest.SummaryItemBean@3d18dcc3[overall=com.atlassian.jira.plugin.devstatus.summary.beans.BranchOverallBean@675aae34[count=0,lastUpdated=<null>,lastUpdatedTimestamp=<null>],byInstanceType={}]},errors=[],configErrors=[]], '
                                  'devSummaryJson={"cachedValue":{"errors":[],"configErrors":[],"summary":{"pullrequest":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":"OPEN","details":{"openCount":0,"mergedCount":0,"declinedCount":0,"total":0},"open":true},"byInstanceType":{}},"build":{"overall":{"count":0,"lastUpdated":null,"failedBuildCount":0,"successfulBuildCount":0,"unknownBuildCount":0},"byInstanceType":{}},"review":{"overall":{"count":0,"lastUpdated":null,"stateCount":0,"state":null,"dueDate":null,"overDue":false,"completed":false},"byInstanceType":{}},"deployment-environment":{"overall":{"count":0,"lastUpdated":null,"topEnvironments":[],"showProjects":false,"successfulCount":0},"byInstanceType":{}},"repository":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}},"branch":{"overall":{"count":0,"lastUpdated":null},"byInstanceType":{}}}},"isStale":false}}',
             'customfield_10100': None,
             'customfield_10104': None,
             'customfield_10105': '0|i001an:',
             'customfield_10200': None,
             'customfield_10202': None,
             'customfield_10203': None,
             'customfield_10204': None,
             'customfield_10205': None,
             'customfield_10206': None,
             'customfield_10208': None,
             'customfield_10209': None,
             'customfield_10210': None,
             'customfield_10211': None,
             'customfield_10300': None,
             'customfield_10301': None,
             'customfield_10302': None,
             'customfield_10303': None,
             'customfield_10304': None,
             'customfield_10305': None,
             'customfield_10306': None,
             'customfield_10307': None,
             'customfield_10308': None,
             'description': None,
             'duedate': None,
             'environment': None,
             'fixVersions': [],
             'issuelinks': [],
             'issuetype': {'avatarId': 10318,
                           'description': 'A task that needs to be done.',
                           'iconUrl': 'http://52.169.187.138:8080/secure/viewavatar?size=xsmall&avatarId=10318&avatarType=issuetype',
                           'id': '10002',
                           'name': 'Task',
                           'self': 'http://52.169.187.138:8080/rest/api/2/issuetype/10002',
                           'subtask': False},
             'labels': [],
             'lastViewed': '2020-06-10T08:52:41.086+0000',
             'priority': {'iconUrl': 'http://52.169.187.138:8080/images/icons/priorities/medium.svg',
                          'id': '3',
                          'name': 'Medium',
                          'self': 'http://52.169.187.138:8080/rest/api/2/priority/3'},
             'progress': {'percent': 100, 'progress': 97200, 'total': 97200},
             'project': {'avatarUrls': {'16x16': 'http://52.169.187.138:8080/secure/projectavatar?size=xsmall&avatarId=10324',
                                        '24x24': 'http://52.169.187.138:8080/secure/projectavatar?size=small&avatarId=10324',
                                        '32x32': 'http://52.169.187.138:8080/secure/projectavatar?size=medium&avatarId=10324',
                                        '48x48': 'http://52.169.187.138:8080/secure/projectavatar?avatarId=10324'},
                         'id': '10103',
                         'key': 'TJAP2',
                         'name': 'ALM-Mock',
                         'projectTypeKey': 'software',
                         'self': 'http://52.169.187.138:8080/rest/api/2/project/10103'},
             'reporter': {'active': True,
                          'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                         '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                         '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                         '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                          'displayName': 'admin',
                          'emailAddress': 'devops@devops.com',
                          'key': 'JIRAUSER10000',
                          'name': 'admin',
                          'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                          'timeZone': 'GMT'},
             'resolution': None,
             'resolutiondate': None,
             'status': {'description': '',
                        'iconUrl': 'http://52.169.187.138:8080/',
                        'id': '10102',
                        'name': 'To Do',
                        'self': 'http://52.169.187.138:8080/rest/api/2/status/10102',
                        'statusCategory': {'colorName': 'blue-gray',
                                           'id': 2,
                                           'key': 'new',
                                           'name': 'To Do',
                                           'self': 'http://52.169.187.138:8080/rest/api/2/statuscategory/2'}},
             'subtasks': [],
             'summary': 'reported task',
             'timeestimate': None,
             'timeoriginalestimate': None,
             'timespent': 97200,
             'timetracking': {'timeSpent': '3d 3h', 'timeSpentSeconds': 97200},
             'updated': '2020-06-10T08:53:42.000+0000',
             'versions': [],
             'votes': {'hasVoted': False,
                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-120/votes',
                       'votes': 0},
             'watches': {'isWatching': True,
                         'self': 'http://52.169.187.138:8080/rest/api/2/issue/TJAP2-120/watchers',
                         'watchCount': 1},
             'worklog': {'maxResults': 20,
                         'startAt': 0,
                         'total': 3,
                         'worklogs': [{'author': {'active': True,
                                                  'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                 '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                 '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                 '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                  'displayName': 'admin',
                                                  'emailAddress': 'devops@devops.com',
                                                  'key': 'JIRAUSER10000',
                                                  'name': 'admin',
                                                  'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                  'timeZone': 'GMT'},
                                       'comment': 'Working on issue TJAP2-120',
                                       'created': '2020-06-10T08:53:29.590+0000',
                                       'id': '10300',
                                       'issueId': '10700',
                                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/10700/worklog/10300',
                                       'started': '2020-06-10T00:00:00.000+0000',
                                       'timeSpent': '1d 1h',
                                       'timeSpentSeconds': 32400,
                                       'updateAuthor': {'active': True,
                                                        'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                       '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                       '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                       '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                        'displayName': 'admin',
                                                        'emailAddress': 'devops@devops.com',
                                                        'key': 'JIRAUSER10000',
                                                        'name': 'admin',
                                                        'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                        'timeZone': 'GMT'},
                                       'updated': '2020-06-10T08:53:29.590+0000'},
                                      {'author': {'active': True,
                                                  'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                 '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                 '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                 '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                  'displayName': 'admin',
                                                  'emailAddress': 'devops@devops.com',
                                                  'key': 'JIRAUSER10000',
                                                  'name': 'admin',
                                                  'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                  'timeZone': 'GMT'},
                                       'comment': 'Working on issue TJAP2-120',
                                       'created': '2020-06-10T08:53:35.623+0000',
                                       'id': '10301',
                                       'issueId': '10700',
                                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/10700/worklog/10301',
                                       'started': '2020-06-09T00:00:00.000+0000',
                                       'timeSpent': '1d 1h',
                                       'timeSpentSeconds': 32400,
                                       'updateAuthor': {'active': True,
                                                        'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                       '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                       '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                       '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                        'displayName': 'admin',
                                                        'emailAddress': 'devops@devops.com',
                                                        'key': 'JIRAUSER10000',
                                                        'name': 'admin',
                                                        'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                        'timeZone': 'GMT'},
                                       'updated': '2020-06-10T08:53:35.623+0000'},
                                      {'author': {'active': True,
                                                  'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                 '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                 '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                 '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                  'displayName': 'admin',
                                                  'emailAddress': 'devops@devops.com',
                                                  'key': 'JIRAUSER10000',
                                                  'name': 'admin',
                                                  'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                  'timeZone': 'GMT'},
                                       'comment': 'Working on issue TJAP2-120',
                                       'created': '2020-06-10T08:53:42.964+0000',
                                       'id': '10302',
                                       'issueId': '10700',
                                       'self': 'http://52.169.187.138:8080/rest/api/2/issue/10700/worklog/10302',
                                       'started': '2020-06-08T00:00:00.000+0000',
                                       'timeSpent': '1d 1h',
                                       'timeSpentSeconds': 32400,
                                       'updateAuthor': {'active': True,
                                                        'avatarUrls': {'16x16': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=16',
                                                                       '24x24': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=24',
                                                                       '32x32': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=32',
                                                                       '48x48': 'https://www.gravatar.com/avatar/52e065a0dff91c8d46866128aa5bf4cd?d=mm&s=48'},
                                                        'displayName': 'admin',
                                                        'emailAddress': 'devops@devops.com',
                                                        'key': 'JIRAUSER10000',
                                                        'name': 'admin',
                                                        'self': 'http://52.169.187.138:8080/rest/api/2/user?username=admin',
                                                        'timeZone': 'GMT'},
                                       'updated': '2020-06-10T08:53:42.964+0000'}]},
             'workratio': -1},
  'id': '10700',
  'key': 'TJAP2-120',
  'self': 'http://52.169.187.138:8080/rest/api/2/issue/10700'}

    # run test to verify that calculate_worklogs isn't empty :
    def test_function_does_not_return_empty_list(self):
        self.assertNotEqual(self.worklogs.__len__(), 0,
                            'The list that hold all jira worklogs is empty.')

    # check that the critical attributes are not None
    def test_list_of_attributes_validation(self):
        self.assertNotEqual(self.worklogs[0][0], None)  # worklogID
        self.assertNotEqual(self.worklogs[0][3], None)  # Quantity
        self.assertNotEqual(self.worklogs[0][4], None)  # ReporterID
        self.assertNotEqual(self.worklogs[0][5], None)  # Date

    def test_story_or_task_id(self):
        worklogs_mock_for_story = jira_exporter.calculate_jira_worklogs([self.worklogs_mock_for_story])
        worklogs_mock_for_task = jira_exporter.calculate_jira_worklogs([self.worklogs_mock_for_task])
        self.assertNotEqual(worklogs_mock_for_story[0][1], None)  # StoryID
        self.assertNotEqual(worklogs_mock_for_task[0][2], None)  # TaskID

    def test_for_loops(self):
        mock = jira_exporter.calculate_jira_worklogs([self.worklogs_mock_for_story, self.worklogs_mock_for_task])
        self.assertEqual(len(mock), 6)
        self.assertEqual(len(jira_exporter.calculate_jira_worklogs([])), 0)

    def test_list_of_attributes_validate_is_none(self):
        no_worklogs_mock = jira_exporter.calculate_jira_worklogs(self.no_worklogs_mock)
        self.assertEqual(no_worklogs_mock, [])  # testing if there is no worklogs

    def test_list_of_attributes_validate_length(self):
        # make sure the project object has 12 attributes
        for worklog in self.worklogs:
            self.assertEqual(worklog.__len__(), 6)

    def test_attributes_are_int(self):
        for worklog in self.worklogs:
            # make partial validation to project id : make sure that the str that hold the it contain ony digits.
            self.assertEqual(all(char.isdigit() for char in worklog[0]), True)  # worklogID
            if worklog[1] is not None:
                self.assertEqual(all(char.isdigit() for char in worklog[1]), True)  # StoryID
            if worklog[2] is not None:
                self.assertEqual(all(char.isdigit() for char in worklog[2]), True)  # TaskID
            self.assertEqual(isinstance(worklog[3], int), True)  # Quantity


class CalculateJiraSprints(unittest.TestCase):
    sprints = jira_exporter.calculate_jira_sprints(jira_exporter.fetch_jira_sprints())
    closed_sprint_mock = {'activatedDate': '2020-05-19T15:08:15.580Z',
  'completeDate': '2020-06-01T09:42:07.869Z',
  'endDate': '2020-06-03T00:08:00.000Z',
  'goal': 'the check if redux works',
  'id': 70,
  'name': "hey its alon's sprint",
  'originBoardId': 39,
  'self': 'http://52.169.187.138:8080/rest/agile/1.0/sprint/70',
  'startDate': '2020-05-19T15:08:00.000Z',
  'state': 'closed'}
    active_sprint_mock = {'activatedDate': '2020-06-01T09:42:20.062Z',
  'endDate': '2020-06-15T18:42:00.000Z',
  'goal': '',
  'id': 37,
  'name': "hey its guy's sprint now",
  'originBoardId': 39,
  'self': 'http://52.169.187.138:8080/rest/agile/1.0/sprint/37',
  'startDate': '2020-06-01T09:42:00.000Z',
  'state': 'active'}
    future_sprint_mock = [{'id': 38,
  'name': 'PMO Sprint 1',
  'originBoardId': 7,
  'self': 'http://52.169.187.138:8080/rest/agile/1.0/sprint/38',
  'state': 'future'}]

    # run test to verify that calculate_sprints isn't empty :
    def test_function_does_not_return_empty_list(self):
        self.assertNotEqual(self.sprints.__len__(), 0,
                            'The list that hold all jira sprints is empty.')

    def test_list_of_attributes_validation(self):
        self.assertNotEqual(self.sprints[0][0], None)  # SprintID
        self.assertNotEqual(self.sprints[0][1], None)  # Name
        self.assertNotEqual(self.sprints[0][2], None)  # StartTime
        self.assertNotEqual(self.sprints[0][3], None)  # EndTime

    def test_active_validate(self):
        closed_sprint_mock = jira_exporter.calculate_jira_sprints([self.closed_sprint_mock])
        active_sprint_mock = jira_exporter.calculate_jira_sprints([self.active_sprint_mock])
        self.assertEqual(closed_sprint_mock[0][5], False)  # is_active
        self.assertEqual(active_sprint_mock[0][5], True)  # is_active
        future_sprint_mock = jira_exporter.calculate_jira_sprints(self.future_sprint_mock)
        self.assertEqual(future_sprint_mock, [])  # testing if the sprint is in the future

    def test_for_loop(self):
        mock = jira_exporter.calculate_jira_sprints([self.closed_sprint_mock, self.active_sprint_mock])
        self.assertEqual(len(mock), 2)
        self.assertEqual(len(jira_exporter.calculate_jira_sprints([])), 0)

    def test_list_of_attributes_validate_length(self):
        # make sure the project object has 12 attributes
        for sprint in self.sprints:
            self.assertEqual(sprint.__len__(), 6)

    def test_attributes_are_int(self):
        for sprint in self.sprints:
            # make partial validation to project id : make sure that the str that hold the it contain ony digits.
            self.assertEqual(isinstance(sprint[0], int), True)  # SprintID
