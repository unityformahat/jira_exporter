import requests
from atlassian import Jira
from jira_exporter import (get_jira, calculate_jira_projects,calculate_jira_projects,fetch_jira_projects)
import pprint
from datetime import datetime, timedelta
#from exporterutilities.DbMannager import DbMannager





#Cycles exporter functions :


def is_project_id_exsist(project_id):
    lis_of_all_projects_id = [project[0] for project in calculate_jira_projects(fetch_jira_projects())]
    if str(project_id) in lis_of_all_projects_id:
        return True
    return False

'''
This function will preform http requesr for Zephyr api
to get all cycles related to project id.
'''
def zephyr_request_get_cycles_by_project_id(projectId):
    jira_obj = get_jira()
    base_url = jira_obj.url
    list_of_cycles = []

    #no parameters spesify
    if projectId == None:
        raise ValueError("None projectId.")
    if int(projectId) < 0 :#invalid number
        raise ValueError("Invalid projectId.")
    if is_project_id_exsist(projectId) == False:
        raise ValueError("The given projectId isnt exsist.")



    try:
        #Zahyper request:
        url = base_url +  "rest/zapi/latest/cycle?projectId=" + str(projectId)
        zephyr_request = requests.get(url, auth=(jira_obj.username, jira_obj.password))
    except ConnectionError as err:
        print("Connection Error: %", err , ', ststus : ',zephyr_request.status_code)

    return zephyr_request.json()

'''
This function return all cycles related to a given project Id.
'''
def get_cycles_by_project_id(main_json):
    list_of_cycles = []
    for first_entry in main_json:
        for out_key in main_json[first_entry][0]:
            if out_key != 'recordsCount' and out_key != '-1':
                start_date = None
                end_date = None
                #check if there is date:
                if main_json[first_entry][0][out_key]['startDate'] != '': start_date = main_json[first_entry][0][out_key]['startDate']
                if main_json[first_entry][0][out_key]['endDate'] != '': end_date = main_json[first_entry][0][out_key]['endDate']
                cycle_obj = (out_key,                                           #cycle_id
                             main_json[first_entry][0][out_key]["name"],        #name
                             main_json[first_entry][0][out_key]['versionId'],   #versionID
                             main_json[first_entry][0][out_key]['projectId'],   #projectID
                             start_date,     #startDate
                             end_date  #endDate
                             )
                list_of_cycles.append(cycle_obj)
    return list_of_cycles



'''
Return list of tuplets , with all cycles from all projects.
'''
def get_all_cycles():
    list_ofcycles = []
    lis_of_all_projects_id = [project[0] for project in calculate_jira_projects(fetch_jira_projects())]
    for project_id in lis_of_all_projects_id:
        project_cycles_list = get_cycles_by_project_id(zephyr_request_get_cycles_by_project_id(project_id))
        list_ofcycles = list_ofcycles + project_cycles_list
    return list_ofcycles






def get_list_of_projects_names():
    return [project[2] for project in calculate_jira_projects(fetch_jira_projects())]  # project[2] - project name



#Execute search exporter functions :
"""
The funciton url fill with parameters as projects names '
to be use in request for ExecuteSearch by zephyr api.
base_url example value - "http://52.169.187.138:8080/"
"""
def get_url_for_zephyr_request_execute_search(list_of_projects_names):
    if list_of_projects_names == None:
        raise ValueError("list_of_projects_names is None")
    if list_of_projects_names == []:
        raise ValueError("Empty list_of_projects_names")
    base_url = get_jira().url

    list_of_projects_names = [project[2] for project in calculate_jira_projects(fetch_jira_projects())]  # project[2] - project name

    url = base_url + """rest/zapi/latest/zql/executeSearch?zqlQuery=project= """
    for i in range(0, len(list_of_projects_names)):
        # if there is only one elemet or we at the last element , then there is no need to add 'OR' key word.
        if i == (len(list_of_projects_names) - 1):
            url = url + """\"""" + list_of_projects_names[i] + """\""""
        else:
            url = url + """\"""" + list_of_projects_names[i] + """\"""" + " OR project = "
    return  url



def zephyr_request_get_all_execution_tests(url = get_url_for_zephyr_request_execute_search(get_list_of_projects_names())):
    if url == None:
        raise ValueError("Invalid url.")


    try:
        # Zehyper request:
        jira_obj = get_jira()
        zephyr_request = requests.get(url, auth=(jira_obj.username, jira_obj.password))
    except ConnectionError as err:
        print("Connection Error: %", err, ', ststus : ', zephyr_request.status_code)
    return zephyr_request.json()


"""
The funciton return a list with all execution tests.
"""
def get_all_execution_tests(main_json = zephyr_request_get_all_execution_tests(get_url_for_zephyr_request_execute_search(get_list_of_projects_names()))):
    list_of_executions = []
    for execution in main_json['executions']:
        execution_obj = (
            execution['id'],              #executionId
            execution['status']['name'] , #executionStatus
            execution['issueId'],         #testId
            execution['cycleId'],         #cycleId
            execution['projectId'],       #projectId
            execution['executedOn']       #executedOn
        )
        list_of_executions.append(execution_obj)
    return list_of_executions











def main():

    print('<'*10 , 'All Cycles' ,  10*'>',"^x^")
    cycles = get_all_cycles()
    print(cycles)

    print('<'*10 , 'All Execution tests',  10*'>',"^x^")
    execution_tests = get_all_execution_tests(zephyr_request_get_all_execution_tests(get_url_for_zephyr_request_execute_search(get_list_of_projects_names())))
    print(execution_tests)

    #Insert to Unity DB
    # sending all info to postgresql
    '''
    dbm = DbMannager()
    dbm.postgressql_execute("cycles", cycles)
    dbm.postgressql_execute("execution_tests",execution_tests)
    dbm.close_connection()
    print('*'*50,'<<< End of zephyr_exporter.py main() >>>','*'*50)
    '''


if __name__ == '__main__':
    main()


